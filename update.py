#! /usr/bin/env python3

import urllib.request
import json


def add_debian_packages(packages):
    import apt
    c = apt.Cache()

    for pkg in packages:
        for pkg_name in pkg['pypi_name'].lower(), pkg['name'].lower():
            if pkg_name.startswith('python-'):
                pkg_name = pkg_name[len('python-'):]
            for prefix in 'python-', 'python3-':
                debian_pkg = c.get(prefix + pkg_name)
                if debian_pkg:
                    l = pkg.setdefault('debian_pkg', [])
                    if debian_pkg.name not in l:
                        l.append(debian_pkg.name)

def add_github_stars(packages):
    import github
    import os
    g = github.Github(os.environ['GITHUB_TOKEN'])

    for pkg in packages:
        github_name = pkg['repo_url']
        if github_name.startswith('https://github.com/'):
            github_name = github_name[len('https://github.com/'):]
            repo = g.get_repo(github_name)
            pkg['repo_stars'] = len(list(repo.get_stargazers()))
        else:
            pkg['repo_stars'] = -1


def add_pypi(packages):
    for pkg in packages:
        try:
            r = urllib.request.urlopen('https://pypi.python.org/pypi/'
                                       + pkg['pypi_name']
                                       + '/json')
            j = json.load(r)
            pkg['pypi_license'] = j['info']['license']
            pkg['pypi_version'] = j['info']['version']
        except urllib.error.HTTPError:
            pkg['pypi_license'] = 'unknown'
            pkg['pypi_version'] = 'unknown'

def write_package_table(fp, packages):
    fp.write("Total {} packages, {} of them are already in Debian\n\n"
             .format(len(packages),
                     len(list(filter(lambda p: 'debian_pkg' in p, packages)))))
    for pkg in packages:
        if pkg['pypi_name'] == 'astropy':
            continue
        fp.write("{debian:1} {stars:4} {api:1} {name}\n".format(
            debian = '*' if 'debian_pkg' in pkg else ' ',
            stars = pkg.get('repo_stars', 0),
            api = 'S' if pkg['stable'] else 'U',
            name = pkg['name']
        ))

    fp.write('''
Columns:
 - Debian Status:
    * package in Debian
 - Github Stars
 - API stability
    S stable
    U unstable
 - Name
''')

def write_todo(fp, packages):
    for pkg in packages:
        if 'debian_pkg' in pkg:
            continue
        fp.write('''* Package name     : {name}
  Version          : {pypi_version}
  Upstream Author  : {maintainer}
* URL              : {home_url}
* License          : {pypi_license}
  Programming lang : Python
  Upstream git     : {repo_url} ({repo_stars} stars)
  Pypi URL         : https://pypi.python.org/pypi/{pypi_name}
  Description      : {description}

'''.format(**pkg))

with urllib.request.urlopen("https://www.astropy.org/affiliated/registry.json") as r:
    registry = json.load(r)

packages = registry['packages']
add_debian_packages(packages)
          
add_pypi(packages)
add_github_stars(packages)
with open('affiliated.json', 'w') as fp:
    json.dump(registry, fp, indent=4)

packages.sort(key=lambda pkg: pkg.get('repo_stars', -1), reverse=True)
with open('affiliated-status.txt', 'w') as fp:
    write_package_table(fp, packages)

with open('TODO', 'w') as fp:
    write_todo(fp, packages)
